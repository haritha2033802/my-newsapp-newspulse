
const newsarticle = document.getElementById("newsCard")
const API_KEY = import.meta.env.VITE_API_KEY



function fetchNews(callback) {
  fetch(`https://gnews.io/api/v4/top-headlines?category=general&lang=en&country=us&max=10&apikey=${API_KEY}`)
  .then(response => {
    if (!response.ok) {
      throw new Error('Error');
    }
    return response.json();
  })
  
  .then(data => {
    callback(data.articles);
  })
  
  .catch(error => {
    console.error('Error fetching news:', error);
    callback([]);
  });
}


function displayNews(articles) {
const newsCards = document.querySelectorAll('.newsCard');
newsCards.forEach((newsCard, index) => {
  const article = articles[index];
  if (article) {
    newsCard.innerHTML = `
      <article>
        <img src="${article.image}" alt="${article.title}">
        <p>${article.description}</p>
      </article>
    `;
  } else {

    
    newsCard.innerHTML = '';
  }
});
}


window.addEventListener('load', () => {
fetchNews(displayNews);
});
